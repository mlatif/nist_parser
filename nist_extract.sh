#!/bin/bash
##############################################################################
##############################################################################
main() {
  HEADER="PHOTON SCATTERING PHOTO- PAIR PRODUCTION TOTAL ATTENUATION ENERGY COHERENT INCOHER. ELECTRIC IN IN WITH WITHOUT ABSORPTION NUCLEAR ELECTRON COHERENT COHERENT FIELD FIELD SCATT. SCATT. (MeV) (cm2/g) (cm2/g) (cm2/g) (cm2/g) (cm2/g) (cm2/g) (cm2/g) ";
  D=1000;  # Facteur de conversion 
  MIN=1;   
  MAX=1000;
  STP=1; 
  UNIT="PIC"; 
  SUFF=""; POKA="cm^2/g";

  B_INF_ELT=1;
  B_SUP_ELT=100; 
  B_INF_RNG=1; 
  B_SUP_RNG=100000000; 
  STOP=0; 
  MSG=""; 
  #############################################################################################
  if [ $# -eq 0 ]; then
    STOP=1; 
    MSG="ERREUR\n\tDonner au moins le numéro atomique X in {$B_INF_ELT,...,$B_SUP_ELT}."
  else
    while [[ $# -gt 0 ]]; do
      # echo "~> Processing argument: $1 [$2]"
      case $1 in
        -e) 
          ELT=$2; 
          shift 2
          ;;
        -min)
          MIN=$2; 
          shift 2
          ;;
        -max)
          MAX=$2; 
          shift 2
          ;;
        -stp)
          STP=$2; 
          shift 2
          ;;
        -barn)
          UNIT="CSBA" 
          SUFF="_barn"
          POKA="barn"
          shift
          ;; 
        -h|--help)
        show_help
          exit 0
          ;;
        *)
          echo "Unknown option $1"
          show_help
          exit 0
          ;;
      esac
      # echo "==> Remaining arguments: $@"
    done
    # Test des entrées
    if [ "$ELT" -lt $B_INF_ELT ] || [ "$ELT" -gt $B_SUP_ELT ] && [ $STOP -ne 1 ]; then
        STOP=1; 
        MSG="ERREUR\n\tContrainte sur le numéro atomique X in {$B_INF_ELT,...,$B_SUP_ELT}\n\tVous avez donné X=$ELT...";  
        show_help
    fi
    if [ "$MIN" -lt $B_INF_RNG ] || [ "$MAX" -gt $B_SUP_RNG ] && [ $STOP -ne 1 ]; then
        STOP=1;
        MSG="ERREUR\n\tContrainte sur l'une des bornes en énergie à tester E in [$B_INF_RNG,$B_SUP_RNG] (keV)\n\tVous avez donné E in [$MIN,$MAX] (keV) ...";
    fi
    if [ "$MIN" -gt "$MAX" ] && [ $STOP -ne 1 ]; then
        STOP=1;
        MSG="ERREUR\n\tContrainte sur les bornes en énergie à tester MIN <= MAX\n\tVous avez donné MIN=$MIN et MAX=$MAX ...";
    fi

    if [ "$STP" -lt 1 ] && [ $STOP -ne 1 ]; then 
        STOP=1;
        MSG="ERREUR\n\tContrainte sur le pas STP >= 1\n\tVous avez donné STP=$STP ...";
    fi
    
  fi
#  return 1;
  if [ $STOP -ne 1 ]; then 
      echo -e "EXTRACTION NIST\n\t~> ELT=$ELT - MIN=$MIN - MAX=$MAX - STP=$STP - UNIT=$UNIT"
      FILENAME="nistdata_${ELT}_${MIN}_${MAX}_kev_rng${SUFF}.txt"
      echo -e "Fichier de sortie: $FILENAME"
      #exit 1
      if [ -f "$FILENAME" ]; then rm $FILENAME; fi
      touch "$FILENAME"
      RNG=$(seq $MIN $STP $MAX); 
      # ATTENTION: seq() ne retourne pas un tableau directement, il faut stocker le résultat de la seq en appliquant des parenthèses
      S=($RNG);
      # Récupérer le nombre d'élément dans une seq
      L=${#S[@]};
      CPT=0;
      for i in $RNG; do
        ##############################################################################
        VAL=$(echo "scale=3 ; $i/$D" |bc -l);
        if [ $i -lt $D ]; then
            VAL="0"$VAL;
        fi
        E="$VAL"
        ##############################################################################
        DATA="ZNum=$ELT&ZSym=&Energies=$E&OutOpt=$UNIT";
        #echo -e $DATA
        #echo " --- "
        ##############################################################################
        OUTPUT=$(curl -s 'https://physics.nist.gov/cgi-bin/Xcom/xcom3_1-t' --data-raw $DATA);
        # echo -e $OUTPUT
        # echo -e "---\nREMOVE PREFIX"
        OUTPUT=${OUTPUT#*"<pre>"}
        # echo -e $OUTPUT
        # echo " ---\nREMOVE SUFFIX"
        OUTPUT=${OUTPUT%"</pre>"*}
        #echo -e $OUTPUT;
        ##############################################################################
        arr_1=($HEADER);
        arr_2=($OUTPUT);
        # echo -e "ARR[HEADER] = ${#arr_1[@]} \t ARR[OTHER] = ${#arr_2[@]}";
        DATA=${arr_2[@]:${#arr_1[@]}:${#arr_2[@]}}
        # echo -e "[$i E=$E (MeV)] \t $DATA"
        echo -e "$DATA" | tr " " "\t " >> $FILENAME;
        PG_MSG="E=$E(MeV)"
        ##############################################################################
        CPT=$(( CPT + 1 ))
        clear; 
        echo -e "X=$ELT \t E in [$MIN,+$STP,$MAX] (keV) \t Unit : $POKA ($UNIT) \t ($CPT/$L)" 
        show_progress $CPT $L $PG_MSG
        MSG="GENERATION DU FICHIER $FILENAME\n\tElément X=$ELT avec E in [$MIN,$MAX] (keV) par pas de $STP \n\tsoit $CPT lignes"
      done
      echo -e $MSG
  else
    echo -e $MSG
    show_help
  fi 

}
##############################################################################
##############################################################################
function show_progress {
    bar_size=40
    bar_char_done="#"
    bar_char_todo="-"
    bar_percentage_scale=2
    current="$1"
    total="$2"
    msg="$3"

    # calculate the progress in percentage 
    percent=$(bc <<< "scale=$bar_percentage_scale; 100 * $current / $total" )
    # The number of done and todo characters
    done=$(bc <<< "scale=0; $bar_size * $percent / 100" )
    todo=$(bc <<< "scale=0; $bar_size - $done" )

    # build the done and todo sub-bars
    done_sub_bar=$(printf "%${done}s" | tr " " "${bar_char_done}")
    todo_sub_bar=$(printf "%${todo}s" | tr " " "${bar_char_todo}")

    # output the bar
    echo -ne "\rProgress : [${done_sub_bar}${todo_sub_bar}] ${percent}% \t ${msg}"

    if [ $total -eq $current ]; then
        echo -e "\nDONE"
    fi
}
##############################################################################
##############################################################################
function show_help {
    echo "############################## HELP ##########################################"
    echo "Usage: $0 -e <ELT> [-min <MIN> -max <MAX> -stp <STP> -barn]"
    echo "Obligatoire:"
    echo -e "\t-e <ELT> : Numéro atomique de l'élément X"
    echo "Options:"
    echo -e "\t-min <MIN> : Borne inférieure de l'énergie à tester (keV) > 1 keV"
    echo -e "\t-max <MAX> : Borne supérieure de l'énergie à tester (keV) < 100000 MeV"
    echo -e "\t-stp <STP> : Pas de l'énergie à tester (keV)"
    echo -e "\t-barn : Utiliser la section efficace de diffusion en barn (1 barn = 10^-24 cm^2)"
    echo -e "Remarque: \n\tpar défaut, les sections efficaces sont données en cm^2/g"; 
    echo "##############################################################################"
}

##############################################################################
##############################################################################
##############################################################################
main "$@"; exit