clear all; 
close all; 
set(0,'DefaultFigureWindowStyle','docked'); 
%
% https://fr.wikipedia.org/wiki/Barn
%
X=54;
binf = 1; 
bsup = 5000;
barn = true;   
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
tok_barn = ""; if (barn), tok_barn = "_barn"; end 
cs_file ="nistdata_"+num2str(X)+"_"+num2str(binf)+"_"+num2str(bsup)+"_kev_rng"+tok_barn+".txt"; 
nist_data = importdata(cs_file,"\t"); 
E_rng = nist_data(:,1); 
R_val = nist_data(:,2); 
C_val = nist_data(:,3); 
P_val = nist_data(:,4); 
PNF_val = nist_data(:,5);   % Pair Production in the Nuclear Field
PEF_val = nist_data(:,6);   % Pair Production in the Electron Field
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
figure; 
    hold on; box on;
    set(gca,'color','none')
    set(gca,'TickLabelInterpreter','Latex');
    set(gca,'YScale','log'); 
    plot(E_rng,R_val,'DisplayName',"Rayleigh"); 
    plot(E_rng,C_val,'DisplayName',"Compton");
    plot(E_rng,P_val,'DisplayName',"Photoeletric");
    plot(E_rng,PNF_val,'DisplayName',"Pair - Nuclear field")
    plot(E_rng,PEF_val,'DisplayName',"Pair - Electron field")
    legend('Location','best','Interpreter','latex','FontSize',14); legend boxoff
    xlabel("Energy [MeV]",'Interpreter','latex','FontSize',14)
    if (barn)
        ylabel("Cross section [barn/atom =  10$^{-24}$cm$^2$]",'Interpreter','latex','FontSize',14)
    else
        ylabel("Cross section [cm$^2$/g]",'Interpreter','latex','FontSize',14)
    end
    title(sprintf("Cross-section for $\\gamma$ interaction with X = %d",X),'Interpreter','latex','FontSize',16)


