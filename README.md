# Nist Parser

Un stupide parseur de données pour réaliser une série de requêtes sur la base de données NIST et récupérer les valeurs de sessions efficaces dans un fichier .txt

## Principe

Au cas où: 

```bash 
$ chmod +x nist_extract.sh
```
Exécution: 
```
"############################## HELP ##########################################"
Usage: ./nist_extract.sh -e <ELT> [-min <MIN> -max <MAX> -stp <STP> -barn]
Obligatoire:"
  -e <ELT> : Numéro atomique de l'élément X
Options:"
  -min <MIN> : Borne inférieure de l'énergie à tester (keV) > 1 keV
  -max <MAX> : Borne supérieure de l'énergie à tester (keV) < 100000 MeV
  -stp <STP> : Pas de l'énergie à tester (keV)
  -barn : Utiliser la section efficace de diffusion en barn (1 barn = 10^-24 cm^2)
Remarque:  
    par défaut, les sections efficaces sont données en cm^2/g 
##############################################################################"
```
avec 
- `-e ELT` le numéro atomique;
    - Obligatoire et compris entre 1 et 100.
- `-min MIN` (keV) la valeur minimale des énergies que l'on souhaite obtenir;
  - Valeur par défaut (si non renseigné): 1 keV;
  - Valeur minimale autorisée: 1 keV.
- `-max MAX` (keV) la valeur maximale des énergies que l'on souhaite obtenir;
  - Valeur par défaut (si non renseigné): 100 keV;
  - Valeur maximale autorisée: 100000000 keV.
- `-stp STP` (keV) l'étape d'incrément entre deux énergies
  - Valeur par défaut (si non renseigné): 1 keV
- `-barn` si l'on veut obtenir les données en barn
  
Retour: Un fichier nommé `nistdata_[X]_[MIN]_[MAX]_kev_rng{_barn}.txt` contenant 
- Autant de ligne que d'énergie demandées; 
- Chaque lignes, contient les données ordonnée comme suit:
 
| **Photon Energy** | **Coherent Scattering** | **Incoherent  Scattering** | **Photoeletric absorption** | **Pair production in Nuclear field** | **Pair production in Electron field** | **Total attenuation with CS** | **Total attenuation w/o CS** |
|:-----------------:|:------------------------:|:--------------------------:|:---------------------------:|:------------------------------------:|:-------------------------------------:|:-----------------------------:|:----------------------------:|
|        MeV        |           cm2/g          |            cm2/g           |            cm2/g            |                 cm2/g                |                 cm2/g                 |             cm2/g             |             cm2/g            |


```bash
$ ./nist_extract.sh 54 1 150 1
X=54 - E in [1,+1,150] (keV)
Progress : [########################################] 100.00%    E=0.150(MeV)
DONE
GENERATION DU FICHIER nistdata_54_1_150_1_kev_rng.txt
        Elément X=54 avec E in [1,150] (keV) par pas de 1 
        soit 150 lignes

$ cat nistdata_54_1_150_1_kev_rng.txt 
1.000E-03       8.458E+00       4.417E-03       9.403E+03       0.000E+00       0.000E+00       9.412E+03       9.403E+03
2.000E-03       7.481E+00       1.285E-02       2.080E+03       0.000E+00       0.000E+00       2.088E+03       2.080E+03
...
```
## Idée 

![MarineGEO circle logo](XCOM_FORM.png "MarineGEO logo")

En allant sur le site du [NIST](https://physics.nist.gov/PhysRefData/Xcom/html/xcom1.html) et plus particulièrement sur ce formulaire [FORM](https://physics.nist.gov/cgi-bin/Xcom/xcom2?Method=Elem&Output2=Hand), on peut obtenir les données de sections efficaces. Des fois, ce site internet plante (mais bon).

En allant sur le FORM et en ouvrant une l'inspecteur HTML en parallèle: 
- Saisir les données (eg. Atomic Symbol = Xe OU Atomic Number = 54)
- Saisir une énergie bidon mais valide exprimée en MeV (eg. 0.001)
- Décocher "Include Standard Grid"
- *liquer sur Submit Information*

En parallèle dans l'inspecteur de commande: 
- Aller dans l'onglet NETWORK
- Regarder le nom des requêtes qui ont été lancées
- Cliquer sur *xcom3_1_t* pour inspecter
- Faire clic droit puis Copy/Copy as cURL

On doit obtenit un truc de ce type

```bash 
curl 'https://physics.nist.gov/cgi-bin/Xcom/xcom3_1-t' \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
H 'Cache-Control: max-age=0' \
H 'Connection: keep-alive' \
H 'Content-Type: application/x-www-form-urlencoded' \
H 'Cookie: _ga=GA1.3.1013593307.1655716964; nmstat=b198eb88-d79d-764a-85c0-999ef69d0c67; _gid=GA1.3.204321771.1671443318; _gid=GA1.2.294224029.1671555720; _ga=GA1.2.1013593307.1655716964; _ga_HEQ0YF2VYL=GS1.1.1671555720.13.1.1671555737.0.0.0; _gat_GSA_ENOR0=1; _gat_GSA_ENOR1=1' \
H 'Origin: https://physics.nist.gov' \
H 'Referer: https://physics.nist.gov/cgi-bin/Xcom/xcom2-t' \
H 'Sec-Fetch-Dest: document' \
H 'Sec-Fetch-Mode: navigate' \
H 'Sec-Fetch-Site: same-origin' \
H 'Sec-Fetch-User: ?1' \
H 'Upgrade-Insecure-Requests: 1' \
H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36' \
H 'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"' \
H 'sec-ch-ua-mobile: ?0' \
H 'sec-ch-ua-platform: "Linux"' \
-data-raw 'ZNum=54&ZSym=&Energies=0.001&OutOpt=PIC' \
-compressed
```

Mais en fait, ce qui nous intéresse peut se résumer en deux lignes: 
```bash
DATA="ZNum=$ELT&ZSym=&Energies=$E&OutOpt=PIC";
OUTPUT=$(curl -s 'https://physics.nist.gov/cgi-bin/Xcom/xcom3_1-t' --data-raw $DATA);
```
Et c'est bien cela que l'on va faire plein de fois pour récupérer les données. C'est tout et ça marche bien. 

## Sources additionnelles de code et mantra git: 
- [Taille de chaines de caractères](https://askubuntu.com/questions/883120/length-of-string-separated-by-spaces)
- [Gestion des tableaux](https://www.tutorialkart.com/bash-shell-scripting/bash-array-slice/#gsc.tab=0)
- [Formatage des sorties](https://stackoverflow.com/questions/54419168/bash-how-to-separate-array-elements-with-tabs-and-print-on-the-same-line)
- [Progress Bar](https://www.baeldung.com/linux/command-line-progress-bar)
- [Fonction main](https://unix.stackexchange.com/questions/449498/call-function-declared-below)

et

```
cd existing_repo
git remote add origin https://gitlab.com/mlatif/nist_parser.git
git branch -M master
git push -uf origin master
```